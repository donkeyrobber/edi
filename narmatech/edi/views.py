from django.shortcuts import render
from django.views.generic import TemplateView, ListView
import edi.models

# Create your views here.
class IndexView(TemplateView):
    template_name = 'home/home.html'

class DashboardView(TemplateView):
    template_name = 'dashboard/dashboard.html'

class DepartmentListView(ListView):
    model = edi.models.Department
    template_name = 'department/list.html'